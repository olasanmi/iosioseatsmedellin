
const routes = [
  {
    path: '/',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue')}
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue')}
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/user/profile.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') }
    ]
  },
  {
    path: '/recovery',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/user/recovery.vue') }
    ]
  },
  {
    path: '/change/password',
    component: () => import('layouts/index.vue'),
    children: [
      { path: '', component: () => import('pages/user/changePasswork.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/dashboard/main.vue'),
    children: [
      { 
        path: 'main',
        component: () => import('pages/dashboard/index.vue'),
        meta: {
          requiresAuth: true
        } 
      },
      {
        path: 'restaurant/filter/:id',
        component: () => import('pages/dashboard/filterRestaurant.vue'),
        meta: {
          requiresAuth: true
        } 
      },
      {
        path: 'filter',
        component: () => import('pages/dashboard/filter.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'foods/filter',
        component: () => import('pages/dashboard/foodFilter.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'lista',
        component: () => import('pages/dashboard/listRestaurant.vue'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]

export default routes
