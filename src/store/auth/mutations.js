export const loginSuccess = (state, data) => {
  state.user = data
  state.userId = data.id
}

export const logout = (state, data) => {
  state.user = []
  state.userId = ''
}

export const setRestaurant = (state, data) => {
  state.restaurantsList = data.data
}

export const setUserIdToEdit = (state, data) => {
	state.userIdToEdit = data
}

export const loginSuccessGooglePluss = (state, data) => {
  state.user = data
  state.userId = data.displayName
}