export default {
  //
  user: JSON.parse(localStorage.getItem('user') || '{}'),
  userId: localStorage.getItem('userId') || '',
  restaurantsList: [],
  userIdToEdit: null
}
