import { getRestaurant } from './../../api/apiCall'

export function getRestaurants ({ commit }) {
	return new Promise ((resolve, reject) => {
		return getRestaurant((response) => {
			commit('setRestaurant', response.data)
			resolve(response)
		}, (err) => {
			reject(err)
		})
	})
}

export function setRestaurant ({ commit }, data) {
	return new Promise ((resolve, reject) => {
		commit('setRestaurant', data)
		resolve(data)
	})
}
