import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

export default async () => {
  Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyCPolFSseMc5kAdItBNLJ3C57tfMIUZy-U',
      libraries: 'places'
    }
  })
}
